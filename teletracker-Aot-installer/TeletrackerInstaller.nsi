!include ReplaceInFile.nsi
!include InstallDotNetUtil.nsi
!include IsDotNetInstalled.nsi
!include LogicLib.nsh

Name "TeleTracker"

OutFile "TeleTracker Installer32bit.exe"
InstallDir $PROGRAMFILES

 Page directory
 Page instfiles
 UninstPage uninstConfirm
 UninstPage instfiles
 
Section "Installer Section"
Call IsDotNETInstalled
   Pop $0
   StrCmp $0 1 +1 0
Call CheckAndDownloadDotNet45
SetOutPath "$INSTDIR\TeleTracker"
File /a /r  "TeleTracker\"
Push {install_directory} 
Push "$INSTDIR\TeleTracker"
Push all
Push all 
Push "$INSTDIR\TeleTracker\app-props.xml"
Call ReplaceInFile

Push {install_directory} 
Push "$INSTDIR\TeleTracker"
Push all
Push all 
Push "$INSTDIR\TeleTracker\RunOnStartUp.bat"
Call ReplaceInFile

CreateShortCut "$DESKTOP\Monitoring Teletracker.lnk" "$INSTDIR\TeleTracker\MonitoringTeletracker.exe"
SectionEnd

Section
WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Run"  "TeleTracker" "$INSTDIR\TeleTracker\RunOnStartUp.bat"
WriteRegStr HKCU "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\layers"  "$INSTDIR\TeleTracker\MonitoringTeletracker.exe"  "RUNASADMIN"

SectionEnd

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrlHistoryLibrary;

namespace DateTime
{
    class Program
    {
 
        static UrlHistoryWrapperClass urlHistory = new UrlHistoryWrapperClass();
        static ArrayList list = new ArrayList();
        static UrlHistoryWrapperClass.STATURLEnumerator enumerator;
        static void Main(string[] args)
        {
            enumerator = urlHistory.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (!enumerator.Current.URL.ToString().Contains("file:"))
                {
                    Console.Out.WriteLine(enumerator.Current.URL.ToString() + "%URL Date Splitter%" + enumerator.Current.LastVisited.ToUniversalTime().Ticks.ToString());
                }
            }
            enumerator.Reset();
           
        }
    }
}

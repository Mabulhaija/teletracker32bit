/**
 * 
 */
package com.colureware.spring;

import org.springframework.core.env.PropertySource;

import com.colureware.configuration.Configuration;

/**
 * @author yazan
 * 
 */
public class ConfigurationPropertySourceWrapper extends PropertySource<Configuration> {

    /**
     * @param name
     * @param source
     */
    public ConfigurationPropertySourceWrapper(String name, Configuration source) {
	super(name, source);
    }

    /**
     * @param name
     */
    public ConfigurationPropertySourceWrapper(String name) {
	super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.core.env.PropertySource#getProperty(java.lang.String)
     */
    @Override
    public Object getProperty(String name) {
	return getSource().getProperty(name);
    }

}

/**
 * 
 */
package com.colureware.configuration;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author yazan
 * 
 */
public interface Configuration {

    public abstract void save() throws IOException;

    public abstract void putProperty(String name, Object value);

    public abstract List getList(String key, List defaultValue);

    public abstract List getList(String key);

    public abstract int getMaxIndex(String key);

    public abstract String[] getStringArray(String key);

    public abstract String getString(String key, String defaultValue);

    public abstract String getString(String key);

    public abstract BigInteger getBigInteger(String key, BigInteger defaultValue);

    public abstract BigInteger getBigInteger(String key);

    public abstract BigDecimal getBigDecimal(String key, BigDecimal defaultValue);

    public abstract BigDecimal getBigDecimal(String key);

    public abstract Short getShort(String key, Short defaultValue);

    public abstract short getShort(String key, short defaultValue);

    public abstract short getShort(String key);

    public abstract Long getLong(String key, Long defaultValue);

    public abstract long getLong(String key, long defaultValue);

    public abstract long getLong(String key);

    public abstract Integer getInteger(String key, Integer defaultValue);

    public abstract int getInt(String key, int defaultValue);

    public abstract int getInt(String key);

    public abstract Float getFloat(String key, Float defaultValue);

    public abstract float getFloat(String key, float defaultValue);

    public abstract float getFloat(String key);

    public abstract Double getDouble(String key, Double defaultValue);

    public abstract double getDouble(String key, double defaultValue);

    public abstract double getDouble(String key);

    public abstract Byte getByte(String key, Byte defaultValue);

    public abstract byte getByte(String key, byte defaultValue);

    public abstract byte getByte(String key);

    public abstract Boolean getBoolean(String key, Boolean defaultValue);

    public abstract boolean getBoolean(String key, boolean defaultValue);

    public abstract boolean getBoolean(String key);

    public abstract Properties getProperties(String key, Properties defaults);

    public abstract Properties getProperties(String key);

    public abstract void addProperty(String key, Object value);

    public abstract Iterator getKeys(String prefix);

    public abstract Iterator getKeys();

    public abstract boolean containsKey(String key);

    public abstract void setListDelimiter(char listDelimiter);

    public abstract char getListDelimiter();

    public abstract Object getProperty(String name);

    public void putAll(Map<String, Object> properties);
}

package com.telelogx.teletracker.xml.maker;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.telelogx.teletracker.browser.history.provider.URLHistory;
import com.telelogx.teletracker.session.info.manger.SessionDescriptionEntity;
import com.telelogx.teletracker.session.info.manger.SnapShotDate;
import com.thoughtworks.xstream.XStream;

/**
 * @author Ahmad
 *
 */
public class XMLPreparerUtil {
    
    private final static Logger     logger	= LoggerFactory.getLogger(XMLPreparerUtil.class);
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE MMM dd hh:mm:ss yyyy");
    private static SimpleDateFormat zoneFormatter = new SimpleDateFormat("'GMT'XXX");
    
    /**
     * @param source
     * @param destenation
     * @throws JAXBException
     */
    public static void makeIndexXML(File source, File destenation) throws JAXBException {
	logger.trace("try to make index XML");
	SessionDescriptionEntity sessionDescriptionEntity = getSessionDescriptionEntityFromFile(source);
	Session session = prepareSessionIndexEntity(sessionDescriptionEntity);
	session.setAgentId(source.getParentFile().getParentFile().getParentFile().getName());
	session.setId(source.getParentFile().getParentFile().getName());
	JAXBContext jaxbContext = JAXBContext.newInstance(Session.class);
	Marshaller marshaller = jaxbContext.createMarshaller();
	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	marshaller.marshal(session, destenation);
    }
    
    /**
     * @param sessionDescriptionEntity
     * @return
     */
    private static Session prepareSessionIndexEntity(SessionDescriptionEntity sessionDescriptionEntity) {
	logger.trace("prepare session index entity");
	Session session = new Session();
	String formattedDate = dateFormatter.format(sessionDescriptionEntity.getStartDate());
	Snapshots snapshots = prepareSnapShot(sessionDescriptionEntity.getSnapShots());
	Urls urls = prepareUrl(sessionDescriptionEntity.getUrlsHistorry());
	session.setPart(sessionDescriptionEntity.getFragmentNumber().toString());
	session.setStart(formattedDate);
	session.setTimez(zoneFormatter.format(sessionDescriptionEntity.getStartDate()));
	session.setUrls(urls);
	session.setSnapshots(snapshots);
	
	return session;
    }
    
    /**
     * @param urlsHistorry
     * @return
     */
    private static Urls prepareUrl(List<URLHistory> urlsHistorry) {
	logger.trace("prepare urls");
	Urls urls = new Urls();
	for (URLHistory urlHistory : urlsHistorry) {
	    Url url = new Url();
	    url.setUrl("<![CDATA[" + urlHistory.getUrl() + "]]>");
	    url.setTime(dateFormatter.format(urlHistory.getTime()));
	    url.setTimez(zoneFormatter.format(urlHistory.getTime()));
	    urls.getUrl().add(url);
	}
	return urls;
    }
    
    /**
     * @param list
     * @return
     */
    private static Snapshots prepareSnapShot(List<SnapShotDate> list) {
	logger.trace("prepare snapshots");
	Snapshots snapshots = new Snapshots();
	for (SnapShotDate snapShotDate : list) {
	    Snapshot snapshot = new Snapshot();
	    snapshot.setName(snapShotDate.snapShotName);
	    snapshot.setTime(dateFormatter.format(snapShotDate.snapShotDate));
	    snapshot.setTimez(zoneFormatter.format(snapShotDate.snapShotDate));
	    snapshots.getSnapshot().add(snapshot);
	}
	return snapshots;
    }
    
    /**
     * @param source
     * @return
     */
    private static SessionDescriptionEntity getSessionDescriptionEntityFromFile(File source) {
	SessionDescriptionEntity sessionEntity = null;
	if (source.exists()) {
	    XStream xmlStreamConfiguration = new XStream();
	    sessionEntity = (SessionDescriptionEntity) xmlStreamConfiguration.fromXML(source);
	}
	return sessionEntity;
    }
    
}

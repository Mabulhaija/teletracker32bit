package com.telelogx.teletracker.xml.maker;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = { "id", "agentId", "start", "timez", "part", "snapshots", "urls" })
public class Session {
    private String    id;
    private String    agentId;
    private String    start;
    private String    part;
    private Snapshots snapshots;
    private Urls      urls;
    private String    timez;
    
    /**
     * @return the timez
     */
    @XmlAttribute
    public String getTimez() {
	return timez;
    }
    
    /**
     * @param timez
     *            the timez to set
     */
    public void setTimez(String timez) {
	this.timez = timez;
    }
    
    /**
     * @return
     */
    public Urls getUrls() {
	return urls;
    }
    
    /**
     * @param urls
     */
    public void setUrls(Urls urls) {
	this.urls = urls;
    }
    
    /**
     * @return
     */
    public Snapshots getSnapshots() {
	return snapshots;
    }
    
    /**
     * @param snapshots
     */
    public void setSnapshots(Snapshots snapshots) {
	this.snapshots = snapshots;
    }
    
    @XmlAttribute
    public String getId() {
	return id;
    }
    
    /**
     * @param id
     */
    public void setId(String id) {
	this.id = id;
    }
    
    @XmlAttribute
    public String getAgentId() {
	return agentId;
    }
    
    /**
     * @param agentId
     */
    public void setAgentId(String agentId) {
	this.agentId = agentId;
    }
    
    @XmlAttribute
    public String getStart() {
	return start;
    }
    
    /**
     * @param start
     */
    public void setStart(String start) {
	this.start = start;
    }
    
    @XmlAttribute
    public String getPart() {
	return part;
    }
    
    /**
     * @param part
     */
    public void setPart(String part) {
	this.part = part;
    }
}

package com.telelogx.teletracker.xml.maker;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ahmad
 *
 */
public class Urls {
    List<Url> url;

    /**
     * @return
     */
    public List<Url> getUrl() {
	if (url == null) {
	    url = new ArrayList<Url>();
	}
	return url;
    }

    /**
     * @param urls
     */
    public void setUrl(List<Url> urls) {
	this.url = urls;
    }

}

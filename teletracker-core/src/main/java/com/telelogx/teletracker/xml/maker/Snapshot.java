package com.telelogx.teletracker.xml.maker;

import javax.xml.bind.annotation.XmlAttribute;

public class Snapshot {
    /**
     * 
     */
    private String name;
    private String time;
    private String timez;
    
    /**
     * @return the timez
     */
    @XmlAttribute
    public String getTimez() {
	return timez;
    }
    
    /**
     * @param timez
     *            the timez to set
     */
    public void setTimez(String timez) {
	this.timez = timez;
    }
    
    @XmlAttribute
    public String getName() {
	return name;
    }
    
    /**
     * @param name
     */
    public void setName(String name) {
	this.name = name;
    }
    
    @XmlAttribute
    public String getTime() {
	return time;
    }
    
    /**
     * @param time
     */
    public void setTime(String time) {
	this.time = time;
    }
    
}

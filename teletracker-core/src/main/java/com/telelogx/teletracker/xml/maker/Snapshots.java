package com.telelogx.teletracker.xml.maker;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ahmad
 *
 */
public class Snapshots {

    /**
     * 
     */
    List<Snapshot> snapshot;

    /**
     * @return
     */
    public List<Snapshot> getSnapshot() {
	if (snapshot == null) {
	    snapshot = new ArrayList<Snapshot>();
	}
	return snapshot;
    }

    /**
     * @param snapShots
     */
    public void setSnapshot(List<Snapshot> snapshot) {
	this.snapshot = snapshot;
    }

}

package com.telelogx.teletracker.xml.maker;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * @author Ahmad
 *
 */
public class Url {
    private String time;
    private String url;
    private String timez;
    
    /**
     * @return the timez
     */
    @XmlAttribute
    public String getTimez() {
	return timez;
    }
    
    /**
     * @param timez
     *            the timez to set
     */
    public void setTimez(String timez) {
	this.timez = timez;
    }
    
    @XmlAttribute
    public String getTime() {
	return time;
    }
    
    /**
     * @param time
     */
    public void setTime(String time) {
	this.time = time;
    }
    
    @XmlValue
    public String getUrl() {
	return url;
    }
    
    /**
     * @param url
     */
    public void setUrl(String url) {
	this.url = url;
    }
    
}

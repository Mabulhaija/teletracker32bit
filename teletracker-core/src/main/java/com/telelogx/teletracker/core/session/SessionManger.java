package com.telelogx.teletracker.core.session;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.colureware.configuration.Configuration;
import com.telelogx.teletracker.core.ftp.impl.FTPTransferJob;
import com.telelogx.teletracker.session.info.manger.SessionInformationManger;

/**
 * @author Ahmad
 *
 */
@Component
public class SessionManger {
    
    @Autowired
    private PersistenceInfoService    persistenceInfoService;
    @Autowired
    private SessionInformationManger  sessionInformationManger;
    @Autowired
    private FTPTransferJob	    ftpTransferJob;
    @Autowired
    private Configuration	     configuration;
    
    private PersistenceInfoEntity     persistenceInfoEntity;
    private Thread		    runningSessionThread;
    private Thread		    ftpUploadThread;
    private Date		      currentSessionStartTime;
    private boolean		   running;
    private InputStream	       configXMLInputStream;
    
    private final Logger	      logger		       = LoggerFactory.getLogger(getClass());
    
    private long		      ftpFrequency		 = 100 * 5L;
    private long		      timeSampleRate	       = 10 * 5L;
    private boolean		   authrized;
    private String		    agentIdToCheck;
    private AgentAutherizationService teleTrackerAuthrizationCheck = new AgentAutherizationService();
    
    public SessionManger() {
    }
    
    /**
     * 
     */
    @PostConstruct
    public void init() {
	logger.trace("load persistance date object");
	if (configuration.getProperty("ftpupload-frequency") != null) {
	    ftpFrequency = Long.parseLong((String) configuration.getProperty("ftpupload-frequency"));
	}
	if (configuration.getProperty("snapshot-frequency") != null) {
	    timeSampleRate = Long.parseLong((String) configuration.getProperty("snapshot-frequency"));
	}
	persistenceInfoEntity = persistenceInfoService.getPersistenceInfoEntity();
	if (persistenceInfoEntity.getCurrentSessionDuration() != 0) {
	    persistenceInfoEntity.setSessionNumber(System.currentTimeMillis());
	    persistenceInfoEntity.setRunningSessionDuration(0);
	    persistenceInfoService.setPersistenceInfoEntity(persistenceInfoEntity);
	}
	
	try {
	    configXMLInputStream = ftpTransferJob.getConfigFileFromFtpServer(persistenceInfoEntity.getAgentId());
	    try {
		updateConfiguration(configXMLInputStream);
	    } catch (ConfigurationException e) {
		logger.error("Configuration Exception", e);
	    }
	} catch (NullPointerException | IOException ex) {
	    logger.error("faield to get configuration file from server", ex);
	}
	
    }
    
    /**
     * @return
     */
    public PersistenceInfoEntity getPersistenceEntityInfo() {
	return persistenceInfoEntity;
    }
    
    /**
     * @param agentId
     * @throws IOException
     */
    public void startSession(String agentId) throws IOException {
	logger.info("start new session for agent : " + agentId);
	currentSessionStartTime = new Date();
	persistenceInfoEntity.setAgentId(agentId);
	persistenceInfoEntity.setCurrentSessionStartTime(currentSessionStartTime);
	persistenceInfoService.setPersistenceInfoEntity(persistenceInfoEntity);
	agentIdToCheck = agentId;
	teleTrackerAuthrizationCheck.setHostIp(configuration.getString("lisense-server.url"));
	init();
	running = true;
	createSessionInRunThread();
	createFTPUploadThread();
	
    }
    
    /**
     * 
     */
    public void stopSession() {
	logger.info("stop session");
	if (currentSessionStartTime != null) {
	    persistenceInfoEntity.setRunningSessionDuration((int) (((new Date()).getTime() - currentSessionStartTime
		    .getTime()) / 1000));
	    sessionInformationManger.packageFragments();
	    persistenceInfoService.setPersistenceInfoEntity(persistenceInfoEntity);
	}
	running = false;
	
    }
    
    /**
     * @param configFile
     * @throws ConfigurationException
     */
    private void updateConfiguration(InputStream configurationInputStream) throws ConfigurationException {
	XMLConfiguration xmlConfiguration = new XMLConfiguration();
	xmlConfiguration.load(configurationInputStream);
	
	configuration.putProperty("snapshot-frequency", xmlConfiguration.getProperty("snapshot-frequency"));
	configuration.putProperty("ftpupload-frequency", xmlConfiguration.getProperty("ftpupload-frequency"));
	configuration.putProperty("ftp-server.ip", xmlConfiguration.getProperty("ftp-server.ip"));
	configuration.putProperty("ftp-server.port", xmlConfiguration.getProperty("ftp-server.port"));
	configuration.putProperty("ftp-server.user-name", xmlConfiguration.getProperty("ftp-server.user-name"));
	configuration.putProperty("ftp-server.password", xmlConfiguration.getProperty("ftp-server.password"));
	try {
	    configuration.save();
	} catch (IOException e) {
	    logger.error("faield to save new configuration from server", e);
	}
    }
    
    /**
     * 
     */
    private void createSessionInRunThread() {
	logger.info("start session thread");
	sessionInformationManger.setSessionNumber(persistenceInfoEntity.getAgentId(),
		persistenceInfoEntity.getSessionNumber());
	runningSessionThread = new Thread(new Runnable() {
	    
	    @Override
	    public void run() {
		int count = 0;
		while (running) {
		    persistenceInfoEntity.setRunningSessionDuration((int) (((new Date()).getTime() - currentSessionStartTime
			    .getTime()) / 1000));
		    persistenceInfoService.setPersistenceInfoEntity(persistenceInfoEntity);
		    sessionInformationManger.takeHistorySnapShot();
		    sessionInformationManger.takeScreenSnapShot();
		    count++;
		    if (count > 10) {
			sessionInformationManger.startNewFragment();
			count = 0;
		    }
		    try {
			Thread.sleep(timeSampleRate);
		    } catch (InterruptedException e) {
			logger.error("", e);
		    }
		}
	    }
	});
	runningSessionThread.start();
    }
    
    /**
     * 
     */
    private void createFTPUploadThread() {
	
	ftpUploadThread = new Thread(new Runnable() {
	    @Override
	    public void run() {
		logger.info("start FTP upload thread");
		
		while (running) {
		    try {
			authrized = teleTrackerAuthrizationCheck.checkAgentAutherization(agentIdToCheck);
		    } catch (IOException e) {
			logger.error("IOexception While get authrization result", e);
		    }
		    if (authrized) {
			try {
			    
			    ftpTransferJob.transferFile(persistenceInfoService.getPersistenceInfoEntity().getAgentId());
			} catch (IOException e) {
			    logger.error("error while transfer data", e);
			}
		    }
		    try {
			Thread.sleep(ftpFrequency);
		    } catch (InterruptedException e) {
			logger.error("", e);
		    }
		}
		
	    }
	    
	});
	
	ftpUploadThread.start();
    }
    
}

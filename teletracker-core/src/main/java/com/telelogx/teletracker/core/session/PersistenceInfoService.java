package com.telelogx.teletracker.core.session;

/**
 * @author Ahmad
 *
 */
public interface PersistenceInfoService {

    /**
     * @return
     */
    PersistenceInfoEntity getPersistenceInfoEntity();

    /**
     * @param sessionEntity
     */
    void setPersistenceInfoEntity(PersistenceInfoEntity sessionEntity);

}

package com.telelogx.teletracker.core.session;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.sun.mail.util.BASE64DecoderStream;

/**
 * @author
 *
 */
public class DESEncrptor {

    private static final String ENC_KEY = "fe34ca11";
    private static final String DES = "DES/ECB/PKCS5Padding";

    private Cipher decryptionCipher;
    private SecretKey teletrackerKey;

    /**
     * @throws InvalidKeySpecException
     * 
     */
    public DESEncrptor() {
	try {
	    byte[] keyBytes = ENC_KEY.getBytes("ASCII");
	    SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
	    teletrackerKey = factory.generateSecret(new DESKeySpec(keyBytes));

	    decryptionCipher = Cipher.getInstance(DES);
	    decryptionCipher.init(Cipher.DECRYPT_MODE, teletrackerKey);

	} catch (NoSuchAlgorithmException | NoSuchPaddingException
		| InvalidKeySpecException | InvalidKeyException
		| UnsupportedEncodingException e) {
	    throw new RuntimeException(e);
	}
    }

    /**
     * @param encryptedData
     * @return
     */
    public String decrypt(String encryptedData) {
	try {
	    byte[] decodedEncryptedText = BASE64DecoderStream
		    .decode(encryptedData.getBytes());
	    byte[] originalText = decryptionCipher
		    .doFinal(decodedEncryptedText);
	    return new String(originalText, "UTF8");
	} catch (IllegalBlockSizeException | BadPaddingException
		| UnsupportedEncodingException e) {
	    throw new RuntimeException(e);
	}
    }

    /**
     * @param args
     * @throws InvalidKeySpecException
     */
    public static void main(String[] args) throws InvalidKeySpecException {
	DESEncrptor t = new DESEncrptor();

    }
}

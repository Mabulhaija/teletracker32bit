package com.telelogx.teletracker.core.UI;

public class ApplicationAlreadyRunningException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
     * 
     */
	public ApplicationAlreadyRunningException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ApplicationAlreadyRunningException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ApplicationAlreadyRunningException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ApplicationAlreadyRunningException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ApplicationAlreadyRunningException(Throwable cause) {
		super(cause);
	}

}

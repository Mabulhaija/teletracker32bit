package com.telelogx.teletracker.core.UI;

import java.util.prefs.Preferences;


/**
 * @author MohammadQasim
 *
 */
public class ApplicationSingleInstanceChecker {

    private final static String APPLICATION_LOCK_REGISTRY_KEY = "telelogx.instance.lock";
    private final static String APPLICATION_LOCK_TIME_REGISTRY_KEY = "telelogx.instance.locktimeout";
    private final static String APPLICATION_LOCKED = "LOCKED";
    private final static String APPLICATION_UNLOCKED = "UNLOCKED";
    private final static int LOCK_TIMEOUT = 5000;

    private Preferences userPreferences = Preferences.userRoot();
    private Boolean applicationRunning = false;
    private String registryKeyPostfix;

    public ApplicationSingleInstanceChecker() {

	this("TeleTrackerRedistryKey");

    }

    /**
     * 
     */
    public ApplicationSingleInstanceChecker(String registryKeyPostfix) {
	if (registryKeyPostfix == null) {
	    throw new IllegalArgumentException("registryKeyPostfix can't be null");
	}
	this.registryKeyPostfix = registryKeyPostfix;
    }

    /**
     * @throws InterruptedException
     */
    public void startInstance() throws InterruptedException {
	if (isLocked()) {
	    throw new ApplicationAlreadyRunningException("Another instance of the application already running");
	} else {
	    lock();
	}
	startLockTimeUpdaterThread();
    }

    /**
     * 
     */
    public void stopInstance() {
	unlock();
    }

    /**
     * 
     */
    private void startLockTimeUpdaterThread() {
	applicationRunning = true;
	Thread updateThread = new Thread(new Runnable() {
	    @Override
	    public void run() {
		while (true) {
		    synchronized (applicationRunning) {
			if (applicationRunning == false) {
			    break;
			}
			userPreferences.put(getActualRegistryKeyName(APPLICATION_LOCK_TIME_REGISTRY_KEY),
				System.currentTimeMillis() + "");
		    }
		    try {
			Thread.sleep(LOCK_TIMEOUT);
		    } catch (InterruptedException e) {
			//
		    }
		}
	    }
	});
	updateThread.start();
    }

    /**
     * @return
     */
    private boolean isLocked() {
	Long currentTime = System.currentTimeMillis();
	String timeSavedInregistryKey = userPreferences.get(
		getActualRegistryKeyName(APPLICATION_LOCK_TIME_REGISTRY_KEY), null);
	String lockedKeyValue = userPreferences.get(getActualRegistryKeyName(APPLICATION_LOCK_REGISTRY_KEY), null);
	if (timeSavedInregistryKey == null || lockedKeyValue == null) {
	    return false;
	}

	long timediff = currentTime - Long.parseLong(timeSavedInregistryKey);
	boolean result = lockedKeyValue.equals(APPLICATION_LOCKED) && timediff <= LOCK_TIMEOUT;
	return result;
    }

    /**
     * 
     */
    private void lock() {
	userPreferences.put(getActualRegistryKeyName(APPLICATION_LOCK_REGISTRY_KEY), APPLICATION_LOCKED);
	userPreferences.put(getActualRegistryKeyName(APPLICATION_LOCK_TIME_REGISTRY_KEY), System.currentTimeMillis()
		+ "");
    }

    /**
     * 
     */
    private void unlock() {
	synchronized (applicationRunning) {
	    applicationRunning = false;
	}
	userPreferences.put(getActualRegistryKeyName(APPLICATION_LOCK_REGISTRY_KEY), APPLICATION_UNLOCKED);
    }

    /**
     * @param registryKey
     * @return
     */
    private String getActualRegistryKeyName(String registryKey) {
	return registryKey + registryKeyPostfix;
    }

    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
	ApplicationSingleInstanceChecker checker = new ApplicationSingleInstanceChecker("test_app");
	checker.startInstance();
	System.out.println("Started");
	Thread.sleep(10000);
	checker.stopInstance();
	System.out.println("Done");
    }
}

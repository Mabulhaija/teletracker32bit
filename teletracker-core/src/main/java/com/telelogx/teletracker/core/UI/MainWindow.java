package com.telelogx.teletracker.core.UI;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import jfx.messagebox.MessageBox;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.colureware.util.StartupUtils;
import com.telelogx.teletracker.core.session.SessionManger;

/**
 * @author Ahmad
 *
 */
public class MainWindow extends Application {
    
    private static final String		     RUNNING_STATUS = "running";
    private static final String		     IDEAL_STATUS   = "ideal";
    // private static final Long TIME_OUT_HOURS = 2 * 60 * 60 * 1000L;
    
    private static ApplicationSingleInstanceChecker applicationSingleInstanceChecker;
    private Image				   logoImage      = new Image("Monitoring_Original_Cropped.png");
    private Text				    scenetitle;
    private TextField			       agentIdTextField;
    private Button				  controlButton;
    private Text				    sessionInfoText;
    private GridPane				grid;
    private EventHandler<ActionEvent>	       startNewSession;
    private EventHandler<ActionEvent>	       stopSessionAction;
    private MenuBar				 menuBar	= new MenuBar();
    private MenuItem				About	  = new MenuItem();
    private Menu				    Main	   = new Menu("File");
    
    private SessionManger			   sessionManger;
    private long				    sessionDuration;
    private String				  sessionStatus  = IDEAL_STATUS;
    private Stage				   primaryStage;
    
    public static void main(String[] args) {
	try {
	    applicationSingleInstanceChecker = new ApplicationSingleInstanceChecker();
	    applicationSingleInstanceChecker.startInstance();
	} catch (InterruptedException e) {
	    
	    e.printStackTrace();
	    
	}
	launch(null);
    }
    
    /**
     * 
     */
    public void prepareMainWindowJobs() {
	
	// exitAfterSessionTimeOut();
	
	startNewSession = new EventHandler<ActionEvent>() {
	    
	    @Override
	    public void handle(ActionEvent arg0) {
		if (agentIdTextField.getText().equals("")) {
		    MessageBox.show(primaryStage, "Empty Agent ID", "Warning", MessageBox.ICON_WARNING
			    | MessageBox.CANCEL);
		} else {
		    try {
			sessionManger.startSession(agentIdTextField.getText());
		    } catch (IOException e) {
			e.printStackTrace();
		    }
		    changeUiStatus(RUNNING_STATUS);
		    
		}
	    }
	};
	stopSessionAction = new EventHandler<ActionEvent>() {
	    
	    @Override
	    public void handle(ActionEvent arg0) {
		sessionManger.stopSession();
		changeUiStatus(IDEAL_STATUS);
	    }
	};
	ClassPathXmlApplicationContext context = StartupUtils
		.startClasspathXMLApplicationContext("spring/app-context.xml");
	
	sessionManger = context.getBean(SessionManger.class);
	agentIdTextField.setText(sessionManger.getPersistenceEntityInfo().getAgentId());
	controlButton.setOnAction(startNewSession);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
	
	this.primaryStage = primaryStage;
	primaryStage.setTitle("Monitoring Teletracker");
	GridPane grid = createMainGripPane();
	createAgentIdLabel(grid);
	createAgentIdInputComponent(grid);
	createControlButton(grid);
	createSessionTimerText(grid);
	createSessionTimerThread();
	Scene scene = new Scene(grid);
	primaryStage.setScene(scene);
	primaryStage.getIcons().add(logoImage);
	primaryStage.setResizable(false);
	createAboutMenue(grid);
	
	prepareMainWindowJobs();
	primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	    @Override
	    public void handle(WindowEvent arg0) {
		
		int Result = MessageBox.show(MainWindow.this.primaryStage, "Are You Sure?\r\n"
			+ "If You Exit,Your recent work will not be logged.", "Exit Confirmation",
			MessageBox.ICON_INFORMATION | MessageBox.YES | MessageBox.NO);
		
		if (Result == MessageBox.YES) {
		    
		    closeTeleTrackerInstance();
		    
		} else {
		    MainWindow.launch(null);
		    
		}
		
	    }
	});
	primaryStage.show();
	
    }
    
    /**
 * 
 */
    private void createSessionTimerThread() {
	Timer timer = new Timer();
	TimerTask timerTask = new TimerTask() {
	    @Override
	    public void run() {
		setTime();
	    }
	    
	    /**
	     * 
	     */
	    private void setTime() {
		if (sessionStatus.equals(RUNNING_STATUS)) {
		    sessionDuration = sessionDuration + 1;
		    Platform.runLater(new Runnable() {
			@Override
			public void run() {
			    updateTimer(sessionDuration);
			}
		    });
		} else {
		    sessionDuration = 0;
		    Platform.runLater(new Runnable() {
			@Override
			public void run() {
			    updateTimer(sessionDuration);
			}
		    });
		}
	    }
	};
	timer.schedule(timerTask, 1000, 1000);
    }
    
    /**
     * @return
     */
    private GridPane createMainGripPane() {
	grid = new GridPane();
	grid.setAlignment(Pos.CENTER);
	grid.setVgap(10);
	grid.setPadding(new Insets(0, 0, 0, 0));
	return grid;
    }
    
    /**
     * @param sessionDuration
     * 
     */
    private void updateTimer(long sessionDuration) {
	long hours = TimeUnit.SECONDS.toHours(sessionDuration);
	long remainMinute = TimeUnit.SECONDS.toMinutes(sessionDuration) - TimeUnit.HOURS.toMinutes(hours);
	long remainSecound = sessionDuration
		- (TimeUnit.MINUTES.toSeconds(remainMinute) + TimeUnit.HOURS.toSeconds(hours));
	if (hours > 0 || remainMinute > 0 || remainSecound > 0) {
	    sessionInfoText.setText("Session Duration " + hours + ":" + remainMinute + ":" + remainSecound);
	} else {
	    sessionInfoText.setText("");
	}
    }
    
    /**
     * @param grid
     */
    private void createSessionTimerText(GridPane grid) {
	sessionInfoText = new Text();
	sessionInfoText.setFont(Font.font("Tahoma", FontWeight.NORMAL, 14));
	grid.add(sessionInfoText, 0, 3);
	grid.setHalignment(sessionInfoText, HPos.LEFT);
    }
    
    /**
     * @param grid
     */
    private void createControlButton(GridPane grid) {
	controlButton = new Button("Start New Session");
	grid.add(controlButton, 0, 2);
	grid.setHalignment(controlButton, HPos.CENTER);
    }
    
    /**
     * @param grid
     */
    private void createAgentIdInputComponent(GridPane grid) {
	agentIdTextField = new TextField();
	agentIdTextField.setPrefColumnCount(30);
	grid.add(agentIdTextField, 0, 1);
	grid.setHalignment(agentIdTextField, HPos.CENTER);
    }
    
    /**
     * @param grid
     */
    private void createAgentIdLabel(GridPane grid) {
	scenetitle = new Text("Agent ID");
	scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 16));
	grid.add(scenetitle, 0, 0);
	grid.setHalignment(scenetitle, HPos.CENTER);
	
    }
    
    private void createAboutMenue(GridPane grid) {
	menuBar.getMenus().add(Main);
	About.setText("About");
	Stage stage = new Stage();
	About.setOnAction(new EventHandler<ActionEvent>() {
	    @Override
	    public void handle(ActionEvent e) {
		GridPane aboutGrid = new GridPane();
		aboutGrid.setAlignment(Pos.CENTER);
		aboutGrid.setHgap(5);
		aboutGrid.setVgap(5);
		aboutGrid.setPadding(new Insets(10, 5, 0, 5));
		Text aboutText1 = new Text("Monitoring for electronic systems\r\n");
		
		Text aboutText2 = new Text("111 Wasfi Al-Tal Street, Al-Bakri Commercial Complex � 309\r\n"
			+ "Amman, Jordan\r\n" + "P.O. Box : 927060 Amman, 11190 Jordan\r\n"
			+ "Tel : 00962 - 6 - 5563959\r\n" + "Fax : 00962 - 6 - 5533881\r\n"
			+ "Email: info@monitoring.jo\r\n" + "Website: www.monitoring.jo\r\n");
		
		Text aboutText3 = new Text("TeleTracker Software\r\n ver. 1.0.0.0");
		
		aboutText1.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		aboutText2.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
		aboutText3.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
		Image image = new Image("Original_NonTransparent.jpg");
		ImageView imageView = new ImageView(image);
		Button exitbutton = new Button();
		exitbutton.setText(" OK ");
		exitbutton.setMaxWidth(Double.MAX_VALUE);
		exitbutton.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		exitbutton.setOnAction(new EventHandler<ActionEvent>() {
		    
		    @Override
		    public void handle(ActionEvent arg0) {
			stage.close();
		    }
		});
		
		aboutGrid.add(imageView, 0, 0, 1, 2);
		aboutGrid.add(aboutText1, 1, 0);
		aboutGrid.add(aboutText2, 1, 1);
		aboutGrid.add(aboutText3, 1, 2);
		aboutGrid.add(exitbutton, 2, 2);
		
		Scene sc = new Scene(aboutGrid);
		
		stage.setScene(sc);
		stage.getIcons().add(logoImage);
		stage.show();
	    }
	});
	Main.getItems().add(About);
	grid.add(menuBar, 0, 0);
	grid.setHalignment(menuBar, HPos.LEFT);
	grid.setValignment(menuBar, VPos.TOP);
	
    }
    
    /**
     * @param uiStatus
     */
    private void changeUiStatus(String uiStatus) {
	switch (uiStatus) {
	    case IDEAL_STATUS:
		controlButton.setText("Start New Session");
		controlButton.setOnAction(startNewSession);
		sessionStatus = IDEAL_STATUS;
		
		break;
	    case RUNNING_STATUS:
		controlButton.setText("End Current Session");
		controlButton.setOnAction(stopSessionAction);
		sessionStatus = RUNNING_STATUS;
		primaryStage.setIconified(true);
		break;
	}
	
    }
    
    /**
	 * 
	 */
    public void closeTeleTrackerInstance() {
	sessionManger.stopSession();
	Platform.exit();
	applicationSingleInstanceChecker.stopInstance();
	System.exit(0);
    }
    
    /**
	 * 
	 */
    // private void exitAfterSessionTimeOut() {
    // Timer timer = new Timer();
    // timer.schedule(new TimerTask() {
    // public void run() {
    // closeTeleTrackerInstance();
    // }
    // }, TIME_OUT_HOURS);
    // }
    
}

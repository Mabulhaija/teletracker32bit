package com.telelogx.teletracker.core.session.Impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.colureware.configuration.Configuration;
import com.telelogx.teletracker.core.session.PersistenceInfoEntity;
import com.telelogx.teletracker.core.session.PersistenceInfoService;
import com.thoughtworks.xstream.XStream;

/**
 * @author Ahmad
 *
 */
@Component
public class PersistenceInfoServiceImpl implements PersistenceInfoService {

    private String configurationFilePath = "Teletracker\\";
    private PersistenceInfoEntity sessionEntity;
    private final String PERSISTENCE_INFO_FILE_NAME = "session.config";

    @Autowired
    Configuration configuration;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @PostConstruct
    private void init() {

	if ((String) configuration.getProperty("app-dir") != null) {
	    configurationFilePath = (String) configuration.getProperty("app-dir");
	}

	File configurationFile = new File(configurationFilePath + PERSISTENCE_INFO_FILE_NAME);
	if (configurationFile.exists()) {
	    XStream xmlStreamConfiguration = new XStream();
	    PersistenceInfoEntity sessionEntity = (PersistenceInfoEntity) xmlStreamConfiguration
		    .fromXML(configurationFile);
	    setPersistenceInfoEntity(sessionEntity);
	} else {
	    PersistenceInfoEntity sessionEntity = firstTimeAppRunning();
	    try {
		Files.createDirectories(Paths.get(configurationFile.getParent()));
		configurationFile.createNewFile();
	    } catch (IOException e) {
		logger.error("Faield to create persistence file", e);
		throw new RuntimeException("Faield to create persistence file");
	    }
	    setPersistenceInfoEntity(sessionEntity);
	}

    }

    @Override
    public PersistenceInfoEntity getPersistenceInfoEntity() {
	return sessionEntity;
    }

    @Override
    public void setPersistenceInfoEntity(PersistenceInfoEntity sessionEntity) {
	this.sessionEntity = sessionEntity;
	saveCurrentSessionState(sessionEntity);
    }

    /**
     * @param sessionEntity
     */
    private void saveCurrentSessionState(PersistenceInfoEntity sessionEntity) {
	File sessionFile = new File(configurationFilePath + PERSISTENCE_INFO_FILE_NAME);
	XStream xmlStreamConfiguration = new XStream();
	String xml = xmlStreamConfiguration.toXML(sessionEntity);
	try {
	    FileUtils.writeStringToFile(sessionFile, xml);
	} catch (IOException e) {
	    logger.error("error while saving session persistence file", e);
	}
    }

    /**
     * 
     */
    private PersistenceInfoEntity firstTimeAppRunning() {
	PersistenceInfoEntity sessionEntity = new PersistenceInfoEntity();
	sessionEntity.setAgentId("");
	sessionEntity.setRunningSessionDuration(0);
	sessionEntity.setSessionNumber(System.currentTimeMillis());
	sessionEntity.setCurrentSessionStartTime(new Date(0));
	return sessionEntity;
    }

}

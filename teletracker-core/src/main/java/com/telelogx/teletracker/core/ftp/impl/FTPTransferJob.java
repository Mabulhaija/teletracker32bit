package com.telelogx.teletracker.core.ftp.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.colureware.configuration.Configuration;

/**
 * @author Yazan
 *
 */
@Component
public class FTPTransferJob {
    
    private final Logger  logger = LoggerFactory.getLogger(getClass());
    
    private String	baseDir;
    private String	host;
    private Integer       port;
    private String	username;
    private String	password;
    
    @Autowired
    private Configuration configuration;
    
    private FTPClient     ftp    = new FTPClient();
    
    /**
     * @throws SocketException
     * @throws IOException
     */
    @PostConstruct
    public void init() {
	try {
	    setHost(configuration.getString("ftp-server.url"));
	    setPort(Integer.parseInt((String) configuration.getProperty("ftp-server.port")));
	    setUsername((String) configuration.getProperty("ftp-server.user-name"));
	    setPassword((String) configuration.getProperty("ftp-server.password"));
	    setBaseDir((String) configuration.getProperty("ftp-dir"));
	    connectToFtpServer();
	} catch (IOException e) {
	    logger.error("Couldn't connect to ftp at initialization", e);
	}
    }
    
    /**
     * @param agentId
     * 
     */
    public void transferFile(String agentId) throws IOException {
	String[] files = new File(baseDir).list();
	if (files != null) {
	    if (files.length == 0) {
		logger.trace("No files to move to ftp");
		return;
	    } else {
		try {
		    ftp.changeWorkingDirectory("/");
		} catch (IOException e) {
		    logger.warn("ftp not connected will try to connect");
		    connectToFtpServer();
		}
	    }
	    
	    for (String file : files) {
		try {
		    ftp.makeDirectory("/" + agentId);
		    // System.out.println(ftp.printWorkingDirectory());
		    ftp.changeWorkingDirectory("/" + agentId);
		    // System.out.println(ftp.printWorkingDirectory());
		    moveFile(file);
		} catch (IOException e) {
		    connectToFtpServer();
		    logger.error("Error while moving file " + file, e);
		}
	    }
	}
	
    }
    
    /**
     * @param agentId
     * @return
     * @throws SocketException
     * @throws IOException
     */
    public InputStream getConfigFileFromFtpServer(String agentId) throws SocketException, IOException {
	logger.debug("getting file from ftp server");
	if (!ftp.getKeepAlive()) {
	    logger.warn("ftp not connected will try to connect and check configration file.");
	    connectToFtpServer();
	}
	InputStream inputStream = ftp.retrieveFileStream(agentId + "/config.properties");
	if (inputStream == null) {
	    inputStream = ftp.retrieveFileStream("config.properties");
	}
	if (inputStream != null) {
	    logger.debug("file got from ftp server");
	} else {
	    throw new IOException("Faield to get Configuration file from server");
	}
	return inputStream;
	
    }
    
    /**
     * @param file
     * @throws IOException
     * @throws SocketException
     */
    protected void moveFile(String file) throws SocketException, IOException {
	
	logger.debug("Moving file to ftp " + file);
	String mediaFile = file.replaceAll(".ready", "");
	// ftp.enterLocalPassiveMode();
	Path mediFilePath = Paths.get(baseDir, mediaFile);
	boolean storageResult;
	try (InputStream in = new FileInputStream(mediFilePath.toFile())) {
	    try {
		storageResult = ftp.storeFile(mediaFile, in);
	    } catch (SocketException | FTPConnectionClosedException e) {
		logger.warn("Erro occured in ftp.storeFile closing ftp connection");
		throw e;
	    }
	    
	    if (!storageResult) {
		throw new IOException(String.format(
			"Couldn't transfer file(%1$s) with error (%2$s), with replay message (%3$s)", mediaFile,
			ftp.getReply(), ftp.getReplyString()));
	    }
	    Paths.get(baseDir, file).toFile().delete();
	    in.close();
	    mediFilePath.toFile().delete();
	}
	logger.debug("File moved to ftp " + file);
	
    }
    
    /**
     * @return
     * @throws IOException
     * @throws SocketException
     */
    private void connectToFtpServer() throws SocketException, IOException {
	try {
	    ftp.disconnect();
	} catch (Exception e) {
	    //
	}
	ftp.connect(host, port);
	ftp.setBufferSize(1024 * 1024);
	ftp.setControlKeepAliveTimeout(10);
	int reply = ftp.getReplyCode();
	if (!FTPReply.isPositiveCompletion(reply)) {
	    ftp.disconnect();
	    throw new IOException("Couldn't connect to ftp server");
	}
	boolean logedIn = ftp.login(username, password);
	if (!logedIn) {
	    throw new IOException("Login failed to ftp server");
	}
	boolean ftpModeChangeSuccess = ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
	if (!ftpModeChangeSuccess) {
	    throw new IOException("Couldn't change file type to binary");
	}
	logger.debug("Connected to FTP Server.");
    }
    
    /**
     * @return the baseDir
     */
    public String getBaseDir() {
	return baseDir;
    }
    
    /**
     * @param baseDir
     *            the baseDir to set
     */
    public void setBaseDir(String baseDir) {
	this.baseDir = baseDir;
    }
    
    /**
     * @return the host
     */
    public String getHost() {
	return host;
    }
    
    /**
     * @param host
     *            the host to set
     */
    public void setHost(String host) {
	this.host = host;
    }
    
    /**
     * @return the port
     */
    public Integer getPort() {
	return port;
    }
    
    /**
     * @param port
     *            the port to set
     */
    public void setPort(Integer port) {
	this.port = port;
    }
    
    /**
     * @return the username
     */
    public String getUsername() {
	return username;
    }
    
    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
	this.username = username;
    }
    
    /**
     * @return the password
     */
    public String getPassword() {
	return password;
    }
    
    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
	this.password = password;
    }
}

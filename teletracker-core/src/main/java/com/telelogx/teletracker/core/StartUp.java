package com.telelogx.teletracker.core;

import com.telelogx.teletracker.core.UI.MainWindow;

public class StartUp {
    
    public static void main(String[] args) {
	
	try {
	    MainWindow.main(args);
	} catch (Exception exception) {
	    String stackTrace = org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(exception);
	    System.out.println(stackTrace);
	}
	
    }
}

package com.telelogx.teletracker.core.session;

import java.util.Date;

/**
 * @author Ahmad
 *
 */
public class PersistenceInfoEntity {

    private String agentId;
    private Long sessionNumber;
    private Integer currentSessionDuration;
    private Date currentSessionStartTime;

    /**
     * @return
     */
    public Date getCurrentSessionStartTime() {
	return currentSessionStartTime;
    }

    /**
     * @param sessionStartTime
     */
    public void setCurrentSessionStartTime(Date currentSessionStartTime) {
	this.currentSessionStartTime = currentSessionStartTime;
    }

    /**
     * @return
     */
    public Integer getCurrentSessionDuration() {
	return currentSessionDuration;
    }

    /**
     * @param sessionDuration
     */
    public void setRunningSessionDuration(Integer currentSessionDuration) {
	this.currentSessionDuration = currentSessionDuration;
    }

    /**
     * @return
     */
    public Long getSessionNumber() {
	return sessionNumber;
    }

    /**
     * @param sessionNumber
     */
    public void setSessionNumber(Long sessionNumber) {
	this.sessionNumber = sessionNumber;
    }

    /**
     * @return
     */
    public String getAgentId() {
	return agentId;
    }

    /**
     * @param agentId
     */
    public void setAgentId(String agentId) {
	this.agentId = agentId;
    }
}

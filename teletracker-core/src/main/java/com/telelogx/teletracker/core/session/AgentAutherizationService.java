package com.telelogx.teletracker.core.session;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.apache.commons.lang.RandomStringUtils;

/**
 * @author Ahmad
 *
 */
public class AgentAutherizationService {
    
    private static final int NUMBER_OF_SALT_CAHR = 5;
    String		   saltedAgnetId;
    DESEncrptor	      desEncrptor	 = new DESEncrptor();
    
    String		   hostIp;
    
    /**
     * @param agentidString
     * @return
     * @throws IOException
     */
    public boolean checkAgentAutherization(String agentidString) throws IOException {
	saltedAgnetId = agentidString + generateSalt();
	String encryptedAuthResult = connectToEncryptionServer("auth", saltedAgnetId);
	String authRes = desEncrptor.decrypt(encryptedAuthResult);
	
	if (saltedAgnetId.equals(authRes)) {
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * @param myAgentIDstring
     * @return
     */
    private String generateSalt() {
	saltedAgnetId = RandomStringUtils.randomAlphabetic(NUMBER_OF_SALT_CAHR);
	return saltedAgnetId;
    }
    
    private String connectToEncryptionServer(String operation, String agent) throws IOException {
	
	URL encryptionServer = new URL("http://" + hostIp + "/?op=" + operation + "&id=" + agent);
	
	BufferedReader in = new BufferedReader(new InputStreamReader(encryptionServer.openStream()));
	
	String inputLine;
	if ((inputLine = in.readLine()) != null)
	
	{
	    in.close();
	    return inputLine;
	} else {
	    System.out.println("Error while get Response.");
	    in.close();
	    return "error";
	}
    }
    
    /**
     * @return the hostIp
     */
    public String getHostIp() {
	return hostIp;
    }
    
    /**
     * @param hostIp
     *            the hostIp to set
     */
    public void setHostIp(String hostIp) {
	this.hostIp = hostIp;
    }
    
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
	AgentAutherizationService t = new AgentAutherizationService();
	System.out.println(t.checkAgentAutherization("test19"));
	
    }
    
}

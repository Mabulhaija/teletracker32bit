package com.telelogx.teletracker.zip.maker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZipFileMakerUtil {

    private final static Logger logger = LoggerFactory
	    .getLogger(ZipFileMakerUtil.class);

    /**
     * @param baseDirFile
     * @param appFtpDir
     * @param zipFileName
     */
   
    public static void createZipFile(File baseDirFile, String appFtpDir,
	    String zipFileName) {
	
	List<File> baseDirContent = Arrays.asList(baseDirFile.listFiles());
	ZipOutputStream zos = null;
	FileOutputStream fileOutputStream = null;
	
	
	try {
	    File zipFile = new File(appFtpDir + zipFileName + ".zip");
	    Files.createDirectories(Paths.get(zipFile.getParent()));
	    zipFile.createNewFile();
	    fileOutputStream = new FileOutputStream(zipFile);
	    zos = new ZipOutputStream(fileOutputStream);
	} catch (IOException e) {
	    logger.error("failed to prepare zip file", e);
	}
	if (fileOutputStream != null && zos != null) {
	    for (File file : baseDirContent) {
		try {
		    if (!file.getName().equals("fragmentDescrition.xml")) {
			addToZipFile(file, zos);
		    }
		} catch (IOException e) {
		    logger.error(
			    "error while trying to add new entry to zip file",
			    e);
		}
	    }
	    try {
		zos.close();
		fileOutputStream.close();
	    } catch (IOException e) {
		logger.error("problem while closing file", e);
	    }

	}
    }

    /**
     * @param file
     * @param zos
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void addToZipFile(File file, ZipOutputStream zos)
	    throws FileNotFoundException, IOException {
	FileInputStream fis = new FileInputStream(file);
	ZipEntry zipEntry = new ZipEntry(file.getName());
	zos.putNextEntry(zipEntry);

	byte[] bytes = new byte[1024];
	int length;
	while ((length = fis.read(bytes)) >= 0) {
	    zos.write(bytes, 0, length);
	}

	zos.closeEntry();
	fis.close();
    }
}

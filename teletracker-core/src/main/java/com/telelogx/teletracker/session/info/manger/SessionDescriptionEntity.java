package com.telelogx.teletracker.session.info.manger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import com.telelogx.teletracker.browser.history.provider.URLHistory;
import com.thoughtworks.xstream.XStream;

/**
 * @author Ahmad
 *
 */
@Configurable
public class SessionDescriptionEntity {
    /**
     * 
     */
    private String agentId;
    private Long sessionNumber;
    private Integer fragmentNumber;
    private Date startDate;
    private Date endDate;
    private List<SnapShotDate> snapShots;
    private List<URLHistory> urlsHistorry;
    private transient File fragmentDescriptionFile;
    private transient String workingDirectory;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private transient String appDir = "Teletracker\\";
    private transient final String DESCRIPTION_FILE_NAME = "fragmentDescrition.xml";

    /**
     * @param agentId
     * @param sessionNumber
     * @param fragmentNumber
     */
    public SessionDescriptionEntity(String agentId, Long sessionNumber, Integer fragmentNumber, String appDir) {
	this.agentId = agentId;
	this.sessionNumber = sessionNumber;
	this.fragmentNumber = fragmentNumber;
	if (appDir != null) {
	    this.appDir = appDir;
	}
	this.workingDirectory = appDir + agentId + "\\" + sessionNumber + "\\" + fragmentNumber + "\\";
	fragmentDescriptionFile = new File(workingDirectory + DESCRIPTION_FILE_NAME);
	try {
	    fragmentDescriptionFile.getParentFile().mkdirs();
	    fragmentDescriptionFile.createNewFile();
	} catch (IOException e) {
	    logger.error("error while create session fragment description file", e);
	    throw new RuntimeException("error while create session fragment description file");
	}
	saveFragmentDescriptionInfo();
    }

    /**
     * @param agentId
     * @param sessionNumber
     * @param fragmentNumber
     */
    public SessionDescriptionEntity() {

    }

    /**
     * @param fragmentDescriptionFile
     */
    public SessionDescriptionEntity(String fragmentDescriptionFileName) {
	fragmentDescriptionFile = new File(fragmentDescriptionFileName);
    }

    /**
     * @return
     */
    public String getWorkingDirectory() {
	return workingDirectory;
    }

    /**
     * @return
     */
    public Long getSessionNumber() {
	return sessionNumber;
    }

    /**
     * @param sessionNumber
     */
    public void setSessionNumber(Long sessionNumber) {
	this.sessionNumber = sessionNumber;
	saveFragmentDescriptionInfo();
    }

    /**
     * @return
     */
    public Integer getFragmentNumber() {
	return fragmentNumber;
    }

    /**
     * @param fragmentNumber
     */
    public void setFragmentNumber(Integer fragmentNumber) {
	this.fragmentNumber = fragmentNumber;
	saveFragmentDescriptionInfo();
    }

    /**
     * @return
     */
    public Date getStartDate() {
	return startDate;
    }

    /**
     * @param startDate
     */
    public void setStartDate(Date startDate) {
	this.startDate = startDate;
	saveFragmentDescriptionInfo();
    }

    /**
     * @return
     */
    public Date getEndDate() {
	return endDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(Date endDate) {
	this.endDate = endDate;
	saveFragmentDescriptionInfo();
    }

    /**
     * @return
     */
    public List<SnapShotDate> getSnapShots() {
	if (snapShots == null) {
	    snapShots = new ArrayList<SnapShotDate>();
	}
	return snapShots;
    }

    /**
     * @param snapShots
     */
    public void setSnapShots(List<SnapShotDate> snapShots) {
	this.snapShots = snapShots;
	saveFragmentDescriptionInfo();
    }

    /**
     * @return
     */
    public List<URLHistory> getUrlsHistorry() {
	if (urlsHistorry == null) {
	    urlsHistorry = new ArrayList<URLHistory>();
	}
	return urlsHistorry;
    }

    /**
     * @param urlsHistorry
     */
    public void setUrlsHistorry(List<URLHistory> urlsHistorry) {
	this.urlsHistorry = urlsHistorry;
	saveFragmentDescriptionInfo();
    }

    /**
     * 
     */
    private void saveFragmentDescriptionInfo() {
	XStream xmlStreamConfiguration = new XStream();
	String xml = xmlStreamConfiguration.toXML(this);
	try {
	    FileUtils.writeStringToFile(fragmentDescriptionFile, xml);
	} catch (IOException e) {
	    logger.error("error while saving session fragment description file", e);
	}
    }

    /**
     * 
     */
    private void loadFragmentDescriptionInfo() {
	XStream xmlStreamConfiguration = new XStream();
	String xml = xmlStreamConfiguration.toXML(this);
	try {
	    FileUtils.writeStringToFile(fragmentDescriptionFile, xml);
	} catch (IOException e) {
	    logger.error("error while saving session fragment description file", e);
	}
    }

}

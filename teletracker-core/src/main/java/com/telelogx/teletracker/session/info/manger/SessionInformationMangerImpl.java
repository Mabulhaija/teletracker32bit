package com.telelogx.teletracker.session.info.manger;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.colureware.configuration.Configuration;
import com.telelogx.teletracker.browser.history.BrowsersHistory;
import com.telelogx.teletracker.browser.history.provider.URLHistory;
import com.telelogx.teletracker.screen.ScreenCapture;
import com.telelogx.teletracker.session.compress.FragmentCompressor;

/**
 * @author Ahmad
 *
 */
@Component
public class SessionInformationMangerImpl implements SessionInformationManger {

    /**
     * 
     */
    private String agentId;
    private Long sessionNumber;
    private Date sessionLastStumpTime;
    private Integer sessionFragmentNumber = 0;
    private SessionDescriptionEntity sessionDescriptionEntity;

    @Autowired
    private ScreenCapture screenCapture;
    @Autowired
    private BrowsersHistory browsersHistory;
    @Autowired
    private Configuration configuration;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void setSessionNumber(String agentId, Long sessionNumber) {
	this.agentId = agentId;
	this.sessionNumber = sessionNumber;
	sessionFragmentNumber = 0;
	sessionDescriptionEntity = new SessionDescriptionEntity(agentId, sessionNumber, sessionFragmentNumber,
		(String) configuration.getProperty("app-dir"));
	sessionLastStumpTime = new Date();
	sessionDescriptionEntity.setStartDate(sessionLastStumpTime);
    }

    @Override
    public void takeScreenSnapShot() {
	logger.trace("try to get screen snapshot");
	List<SnapShotDate> snapShots = sessionDescriptionEntity.getSnapShots();
	screenCapture.setOutputPath(sessionDescriptionEntity.getWorkingDirectory());
	screenCapture.captureScreen();

	String lastSnapShotName = screenCapture.getLastSnapShotFileName();
	SnapShotDate snapShotDate = new SnapShotDate();
	snapShotDate.snapShotDate = new Date();
	snapShotDate.snapShotName = (new File(lastSnapShotName)).getName();

	snapShots.add(snapShotDate);
	sessionDescriptionEntity.setSnapShots(snapShots);
    }

    @Override
    public void takeHistorySnapShot() {
	logger.trace("try to get history snapshot");
	List<URLHistory> urlsHistory = browsersHistory.getHistory(sessionLastStumpTime);
	List<URLHistory> sessionURLHistory = sessionDescriptionEntity.getUrlsHistorry();

	sessionURLHistory.addAll(urlsHistory);
	sessionDescriptionEntity.setUrlsHistorry(sessionURLHistory);

	sessionLastStumpTime = new Date();

    }

    @Override
    public void packageFragments() {
	logger.trace("try to do package fragment");
	sessionLastStumpTime = new Date();
	sessionDescriptionEntity.setEndDate(sessionLastStumpTime);
	(new FragmentCompressor(sessionDescriptionEntity.getWorkingDirectory(),
		(String) configuration.getProperty("ftp-dir"))).init();
    }

    @Override
    public void startNewFragment() {
	logger.trace("try to start new fragment");
	packageFragments();
	sessionFragmentNumber = sessionFragmentNumber + 1;
	sessionDescriptionEntity = new SessionDescriptionEntity(agentId, sessionNumber, sessionFragmentNumber,
		(String) configuration.getProperty("app-dir"));
	sessionDescriptionEntity.setStartDate(new Date());
    }
}

package com.telelogx.teletracker.session.info.manger;

/**
 * @author Ahmad
 *
 */
public interface SessionInformationManger {

    /**
     * @param sessionNumber
     */
    public void setSessionNumber(String agentId, Long sessionNumber);

    /**
     * 
     */
    public void takeScreenSnapShot();

    /**
     * 
     */
    public void takeHistorySnapShot();

    /**
     * 
     */
    public void packageFragments();

    /**
     * 
     */
    public void startNewFragment();

}

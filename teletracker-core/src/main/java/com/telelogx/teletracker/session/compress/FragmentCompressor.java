package com.telelogx.teletracker.session.compress;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import com.telelogx.teletracker.xml.maker.XMLPreparerUtil;
import com.telelogx.teletracker.zip.maker.ZipFileMakerUtil;

/**
 * @author Ahmad
 *
 */
@Configurable
public class FragmentCompressor {
    
    /**
     * 
     */
    private Thread       compressThread;
    private String       appDir = "TempFile\\";
    private String       ftpDir = "ftp\\";
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    /**
     * 
     */
    public FragmentCompressor(final String appDir, final String ftpDir) {
	if (appDir != null) {
	    this.appDir = appDir;
	}
	if (ftpDir != null) {
	    this.ftpDir = ftpDir;
	}
	Runnable compress = new Runnable() {
	    @Override
	    public void run() {
		logger.trace("Fragment compress Thread start");
		prepareXMLFile(new File(appDir + "fragmentDescrition.xml"), new File(appDir + "index.xml"));
		prepareZipFile(new File(appDir), ftpDir);
	    }
	};
	compressThread = new Thread(compress);
    }
    
    /**
     * @param baseDir
     * @param appFtpDir
     */
    private void prepareZipFile(File baseDir, String appFtpDir) {
	logger.trace("start preparing zip file");
	ZipFileMakerUtil.createZipFile(baseDir, appFtpDir, (new Long((new Date()).getTime())).toString());
	removeBaseDir(baseDir);
    }
    
    /**
     * @param baseDir
     */
    private void removeBaseDir(File baseDir) {
	try {
	    FileUtils.deleteDirectory(baseDir);
	} catch (IOException e) {
	    logger.error("Faield to delete temp dir", e);
	    
	}
    }
    
    /**
     * @param fragmentsDescriptionFile
     * @param aPP_FTP_DIR
     */
    private void prepareXMLFile(File fragmentsDescriptionFile, File zipDescriptionFile) {
	logger.trace("start preparing xml file");
	if (fragmentsDescriptionFile.exists()) {
	    try {
		XMLPreparerUtil.makeIndexXML(fragmentsDescriptionFile, zipDescriptionFile);
	    } catch (JAXBException e) {
		logger.error("Error while prepare Index XML file ", e);
	    }
	}
	
    }
    
    /**
     * 
     */
    public void init() {
	File baseDir = new File(appDir);
	compressThread.start();
    }
}

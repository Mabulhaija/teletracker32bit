package com.telelogx.teletracker.screen;

public class ScreenCaptureException extends RuntimeException {

    /**
     * @param message
     */
    public ScreenCaptureException(String message) {
	super(message);
    }

    /**
     * @param message
     */
    public ScreenCaptureException(String message, Throwable cause) {
	super(message, cause);
    }

}

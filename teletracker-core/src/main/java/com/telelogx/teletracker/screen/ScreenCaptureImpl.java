package com.telelogx.teletracker.screen;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.colureware.configuration.Configuration;
import com.colureware.util.StartupUtils;

/**
 * @author Ahmad
 *
 */
@Component
public class ScreenCaptureImpl implements ScreenCapture {
    
    ImageWriter	      writer;
    Iterator		 iter;
    ImageOutputStream	ios;
    ImageWriteParam	  param;
    private Dimension	screenSize;
    private String	   outputPath	= "";
    private String   lastSnapShotFileName;
    private int	      oldheight;
    private int	      oldwidth;
    private float quality;
    private int fixedImageWidth;
    
    @Autowired
    private Configuration configuration;
    
    /**
     * 
     */
    private final Logger     logger	    = LoggerFactory.getLogger(getClass());
    
    public ScreenCaptureImpl() {
	
    }
    
    /**
     * @param outputPath
     */
    public ScreenCaptureImpl(String outputPath) {
	this.outputPath = outputPath;
    }
    
    /**
     * 
     */
    @Override
    @PostConstruct
    public void init() {
	logger.trace("Screen capture init method");
	screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	try{
	setFixedImageWidth(Integer.parseInt((String) configuration
		    .getProperty("Image.Width")));
	}catch(NumberFormatException e){
	    setFixedImageWidth(320);
	    logger.warn("Width set to default,no width in app-props");
	}
	
	try{
	setQuality(Float.parseFloat((String)configuration.getProperty("Image.Quality")));
	}catch(NumberFormatException e){
	    setQuality(0.9f);
	    logger.warn("Quality set to default,no Quality in app-props");
	}
	if(getQuality()==0.0f){
	setQuality(0.9f);
	logger.debug("can't set qualit to 0");
	}
	}
    
    
    /**
     * 
     */
    @Override
    public void captureScreen() {
	
	logger.trace("try to take screen capture");
	try {
	    param = new JPEGImageWriteParam(null);
	    param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	   param.setCompressionQuality(getQuality());
	    BufferedImage screencapture = new Robot().createScreenCapture(new Rectangle(screenSize));
	    screencapture = resizeImage(screencapture);
	   
	     lastSnapShotFileName = outputPath + (new Date()).getTime() + ".jpg";
	    File file = new File(lastSnapShotFileName);
	   writer = null;
	    iter = ImageIO.getImageWritersByFormatName("jpg");
	    if (iter.hasNext())
		writer = (ImageWriter) iter.next();
	   ios = ImageIO.createImageOutputStream(file);
	    writer.setOutput(ios);
	    writer.write(null, new IIOImage(screencapture, null, null), param);

	} catch (AWTException | IOException e) {
	    logger.error("Error while take snapShot", e);
	    throw new ScreenCaptureException("Error while take snapShot", e);
	}
	
    }
    
    /**
     * @return
     */
    @Override
    public String getLastSnapShotFileName() {
	return lastSnapShotFileName;
    }
    
    /**
     * @param outputPath
     */
    @Override
    public void setOutputPath(String outputPath) {
	this.outputPath = outputPath;
    }
    
    /**
     * @param image
     * @return
     * @throws IOException
     */
    private BufferedImage resizeImage(BufferedImage originalImage) throws IOException {
	oldwidth = originalImage.getWidth();
	oldheight = originalImage.getHeight();
	int newWidth = getFixedImageWidth();
	int newHeight = calculateResizedHeight(originalImage);
	
	if(newWidth>oldwidth || newWidth==0){
	    newWidth=oldwidth;
	    setFixedImageWidth(oldwidth);
	    logger.debug("can't set width to 0 or more than screen width ");
	}
	
	BufferedImage resizedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
	Graphics2D graphics2d = resizedImage.createGraphics();
	graphics2d.drawImage(originalImage, 0, 0, newWidth, newHeight, null);
	graphics2d.dispose();
	return resizedImage;
	
    }
    
    /**
     * @param width
     * @param height
     * @return
     */
    private int calculateResizedHeight(BufferedImage originalImage) {
	
	if(oldwidth < getFixedImageWidth()){
	    return oldheight;
	}
	double tempheight = (double) (oldheight * getFixedImageWidth()) / (double) oldwidth;
	tempheight = Math.ceil(tempheight);
	return (int) tempheight;
    }
    public int getFixedImageWidth() {
        return fixedImageWidth;
    }

    public void setFixedImageWidth(int fixedImageWidth) {
        this.fixedImageWidth = fixedImageWidth;
    }
    public float getQuality() {
        return quality;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }
    /**
     * @param args
     * @throws AWTException
     * @throws IOException
     */
    public static void main(String[] args) throws AWTException, IOException {
	ScreenCaptureImpl screenCaptureImpl = new ScreenCaptureImpl("C:\\Users\\telelogx-pc\\Desktop\\newnew.jpg");
	screenCaptureImpl.init();
	screenCaptureImpl.captureScreen();
	
    }
    
}

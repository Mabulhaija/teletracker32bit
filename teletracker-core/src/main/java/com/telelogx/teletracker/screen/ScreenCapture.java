package com.telelogx.teletracker.screen;

/**
 * @author Ahmad
 *
 */
public interface ScreenCapture {

    public void init();

    public void captureScreen();

    public String getLastSnapShotFileName();

    public void setOutputPath(String outputPath);

}

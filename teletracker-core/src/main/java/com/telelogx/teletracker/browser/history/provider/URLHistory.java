package com.telelogx.teletracker.browser.history.provider;

import java.util.Date;

public class URLHistory {

    private String url;
    private Date time;

    /**
     * @return
     */
    public String getUrl() {
	return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
	this.url = url;
    }

    /**
     * @return
     */
    public Date getTime() {
	return time;
    }

    /**
     * @param time
     */
    public void setTime(Date time) {
	this.time = time;
    }

}

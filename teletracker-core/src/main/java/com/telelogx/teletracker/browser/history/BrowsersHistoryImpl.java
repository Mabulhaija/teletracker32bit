package com.telelogx.teletracker.browser.history;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.colureware.configuration.Configuration;
import com.telelogx.teletracker.browser.history.provider.BrowserHistory;
import com.telelogx.teletracker.browser.history.provider.URLHistory;
import com.telelogx.teletracker.browser.history.provider.chrome.ChromeBrowserHistoryImpl;
import com.telelogx.teletracker.browser.history.provider.exception.BrowserHistoryException;
import com.telelogx.teletracker.browser.history.provider.firefox.FireFoxBrowserHistoryImpl;
import com.telelogx.teletracker.browser.history.provider.ie8.InternetExplorerHistoryImpl;

/**
 * @author Ahmad
 *
 */
@Component
public class BrowsersHistoryImpl implements BrowsersHistory {

    private List<BrowserHistory> browsersHistory;
    private Date initTime = new Date();

    @Autowired
    private Configuration configuration;
    /**
     * 
     */
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    @PostConstruct
    public void init() {
	browsersHistory = new ArrayList<BrowserHistory>();
	browsersHistory.add(new ChromeBrowserHistoryImpl());
	browsersHistory.add(new FireFoxBrowserHistoryImpl());
	browsersHistory.add(new InternetExplorerHistoryImpl(configuration
		.getString("browser.history.ieutil")));
	for (BrowserHistory browser : browsersHistory) {
	    browser.init();
	}
    }

    @Override
    public List<URLHistory> getHistory() {
	List<URLHistory> urlHistorys = new ArrayList<URLHistory>();
	for (BrowserHistory browser : browsersHistory) {
	    try {
		if (browser.getBrowserReadingHistoryAbility()) {
		    urlHistorys.addAll(browser.getBrowserHistory(initTime));
		}
	    } catch (BrowserHistoryException ex) {
		logger.error(ex.getMessage(), ex);
	    }
	}
	return urlHistorys;
    }

    @Override
    public List<URLHistory> getHistory(Date startTime) {
	return getHistory(startTime, new Date());
    }

    @Override
    public List<URLHistory> getHistory(Date startTime, Date endTime) {

	List<URLHistory> urlsHistory = getHistory();
	List<URLHistory> filterUrlsHistory = new ArrayList<URLHistory>();

	Date capturedUrlTime = startTime;
	for (URLHistory urlHistory : urlsHistory) {

	    if (urlHistory.getTime().after(capturedUrlTime)) {
		if (urlHistory.getTime().before(endTime)) {
		    filterUrlsHistory.add(urlHistory);
		    capturedUrlTime = filterUrlsHistory.get(
			    filterUrlsHistory.size() - 1).getTime();
		}
	    }
	}

	return filterUrlsHistory;
    }

    public static void main(String[] args) {
	BrowsersHistory browsersHistory = new BrowsersHistoryImpl();
	browsersHistory.init();
	Date testDate = new Date();
	Calendar c = Calendar.getInstance();
	c.setTime(testDate);
	c.add(Calendar.HOUR, -12);
	testDate = c.getTime();
	for (URLHistory uRLHistory : browsersHistory.getHistory(testDate)) {
	    System.out.println(uRLHistory.getTime());
	    System.out.println(uRLHistory.getUrl());
	}
    }

}

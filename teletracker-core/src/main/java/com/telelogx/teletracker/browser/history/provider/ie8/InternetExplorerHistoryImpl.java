package com.telelogx.teletracker.browser.history.provider.ie8;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.telelogx.teletracker.browser.history.provider.BrowserHistory;
import com.telelogx.teletracker.browser.history.provider.URLHistory;
import com.telelogx.teletracker.browser.history.provider.exception.BrowserHistoryException;

/**
 * @author Ahmad
 *
 */
public class InternetExplorerHistoryImpl implements BrowserHistory {
    private final static long DOT_NET_1970 = 621355968000000000L;
    private String pathToExeIeHistoryUtilFile = "";
    private boolean isValid;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 
     */
    public InternetExplorerHistoryImpl() {

    }

    /**
     * @param pathToExeIeHistoryUtilFile
     */
    public InternetExplorerHistoryImpl(String pathToExeIeHistoryUtilFile) {
	this.pathToExeIeHistoryUtilFile = pathToExeIeHistoryUtilFile;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.telelogx.project.browser.history.wrapper.BrowserHistoryWrapper#init()
     */
    @Override
    public void init() {
	logger.trace("IE driver start");

	checkIeExeUtilFileAndLib();
    }

    @Override
    @SuppressWarnings("deprecation")
    public List<URLHistory> getBrowserHistory(Date startTime) {
	Process process;
	List<String> consoleLines = new ArrayList<String>();

	logger.trace("IE try to get history");
	try {
	    process = Runtime.getRuntime().exec(
		    pathToExeIeHistoryUtilFile + "\\DateTime.exe");
	    consoleLines = readAllOutputLines(process);
	} catch (IOException e) {
	    logger.error("error while execute Ie history read", e);
	    throw new BrowserHistoryException("I/O error", e.getCause());
	}
	List<URLHistory> urlHistoryContents = new ArrayList<URLHistory>();
	for (String line : consoleLines) {
	    String[] lineContent = line.split("%URL Date Splitter%");
	    if (lineContent.length == 2) {
		URLHistory urlHistory = new URLHistory();
		urlHistory.setUrl(lineContent[0]);
		try {
		    urlHistory.setTime((prepareDate(lineContent[1])));
		} catch (ParseException e) {
		    logger.error("Unable to convert date string to date", e);
		    throw new BrowserHistoryException(
			    "Unable to convert date string to date",
			    e.getCause());
		}
		urlHistoryContents.add(urlHistory);
	    } else {
		logger.error("Error while reading return value from IE history console");
		throw new BrowserHistoryException(
			"Error while reading return value from IE history console");
	    }
	}
	return urlHistoryContents;
    }

    /**
     * @param string
     * @return
     * @throws ParseException
     */
    private Date prepareDate(String string) throws ParseException {
	Long LongJavaDate = Long.parseLong(string);
	LongJavaDate = LongJavaDate-DOT_NET_1970;
	LongJavaDate =LongJavaDate /(10 * 1000);
	Date date=new Date(LongJavaDate);
	DateFormat format = new SimpleDateFormat("M/d/yyyy K:m:s a",Locale.ENGLISH);
	format.format(date);
	return date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.telelogx.project.browser.history.wrapper.BrowserHistoryWrapper#isValid
     * ()
     */
    @Override
    public Boolean getBrowserReadingHistoryAbility() {
	return isValid;
    }

    /**
     * @param historyFilePath2
     * @return
     */
    private void checkIeExeUtilFileAndLib() {
	isValid = false;
	if (new File(pathToExeIeHistoryUtilFile + "\\DateTime.exe").exists()) {
	    if (new File(pathToExeIeHistoryUtilFile + "\\UrlHistoryLibrary.dll")
		    .exists()) {
		isValid = true;
	    }
	}
    }

    /**
     * @param process
     * @return
     * @throws IOException
     */
    private List<String> readAllOutputLines(Process process) throws IOException {
	ArrayList<String> readedLineList = new ArrayList<String>();
	try (BufferedReader input = new BufferedReader(new InputStreamReader(
		process.getInputStream()))) {
	    String result = "";
	    String line;
	    while ((line = input.readLine()) != null) {
		readedLineList.add(line);
	    }
	    return readedLineList;
	}
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	InternetExplorerHistoryImpl internetExplorerHistoryImpl = new InternetExplorerHistoryImpl(
		"E:\\DEV\\teletracker\\teletracker-Aot-installer\\ieUtil");
	internetExplorerHistoryImpl.init();
	List<URLHistory> urlsHistory = internetExplorerHistoryImpl
		.getBrowserHistory(new Date());
	for (URLHistory uRLHistory : urlsHistory) {
	    System.out.println(uRLHistory.getTime());
	    System.out.println(uRLHistory.getUrl());
	}
    }

}

package com.telelogx.teletracker.browser.history.provider.firefox;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.telelogx.teletracker.browser.history.provider.BrowserHistory;
import com.telelogx.teletracker.browser.history.provider.URLHistory;
import com.telelogx.teletracker.browser.history.provider.exception.BrowserHistoryException;

/**
 * @author Ahmad
 *
 */
public class FireFoxBrowserHistoryImpl implements BrowserHistory {
    private String		historyFilePath;
    private boolean	       isValid;
    private File		  historyFile;
    private File		  tempHistoryFile;
    private FireFoxHistoryService fireFoxHistoryService;
    private String		fireFoxProfileName;
    private final Logger	  logger = LoggerFactory.getLogger(getClass());
    
    /**
     * 
     */
    public FireFoxBrowserHistoryImpl() {
	
	getMozillaProfileName();
	
	this.historyFilePath = System.getProperty("user.home") + "\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\"
		+ this.fireFoxProfileName + "\\places.sqlite";
    }
    
    private void getMozillaProfileName() {
	File historyPath = new File(
		(System.getProperty("user.home") + "\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\"));
	if (historyPath.exists()) {
	    File listOfProfilesNames[] = historyPath.listFiles();
	    for (File defaultName : listOfProfilesNames) {
		if (defaultName.isDirectory() && defaultName.getName().contains(".default")) {
		    this.fireFoxProfileName = defaultName.getName().toString();
		}
	    }
	}
    }
    
    /**
     * @param historyFilePath
     */
    public FireFoxBrowserHistoryImpl(String historyFilePath) {
	this.historyFilePath = historyFilePath;
    }
    
    @Override
    public void init() {
	logger.trace("FireFox driver start");
	try {
	    Class.forName("org.sqlite.JDBC");
	} catch (ClassNotFoundException e) {
	    logger.error("Faield to load SQLite driver", e);
	    throw new BrowserHistoryException("Faield to load SQLite driver");
	}
	checkFireFoxHistoryFile(historyFilePath);
    }
    
    @Override
    public List<URLHistory> getBrowserHistory(Date startTime) {
	List<URLHistory> urlsHistory = new ArrayList<URLHistory>();
	logger.trace("FireFox try to get history");
	try {
	    tempHistoryFile = createTmpHistoryFile(historyFile);
	} catch (IOException exception) {
	    logger.error("Faield while making copy for history file", exception);
	    throw new BrowserHistoryException("Faield while making copy for history file", exception.getCause());
	}
	try {
	    fireFoxHistoryService = new FireFoxHistoryService(tempHistoryFile);
	    urlsHistory = fireFoxHistoryService.getHistory(startTime);
	} catch (SQLException e) {
	    logger.error("Faield while reading history file", e);
	    throw new BrowserHistoryException("Faield while reading history file", e.getCause());
	}
	return urlsHistory;
    }
    
    @Override
    public Boolean getBrowserReadingHistoryAbility() {
	return isValid;
    }
    
    /**
     * @param sourceFile
     * @return
     * @throws IOException
     */
    private File createTmpHistoryFile(File sourceFile) throws IOException {
	tempHistoryFile = new File(sourceFile.toString() + "_tmp");
	Files.copy(sourceFile.toPath(), tempHistoryFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	return tempHistoryFile;
    }
    
    /**
     * @param historyFilePath2
     * @return
     */
    private void checkFireFoxHistoryFile(String historyFilePath) {
	isValid = false;
	historyFile = new File(historyFilePath);
	if (historyFile.exists()) {
	    isValid = true;
	}
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
	
	FireFoxBrowserHistoryImpl fireFoxBrowserHistoryImpl = new FireFoxBrowserHistoryImpl("C:\\Users\\"
		+ System.getenv("USERNAME")
		+ "\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\t04fzboh.default\\places.sqlite");
	fireFoxBrowserHistoryImpl.init();
	Date date = new Date();
	date.setHours(0);
	System.out.println(fireFoxBrowserHistoryImpl.getBrowserReadingHistoryAbility());
	for (URLHistory urlHistory : fireFoxBrowserHistoryImpl.getBrowserHistory(date)) {
	    System.out.println(urlHistory.getTime());
	    System.out.println(urlHistory.getUrl());
	}
	
    }
    
}

package com.telelogx.teletracker.browser.history.provider.chrome;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.telelogx.teletracker.browser.history.provider.URLHistory;

/**
 * @author Ahmad
 *
 */
public class ChromeHistoryDAO {

    private Connection connection;
    private ResultSet resultSet;

    private final String preparedStatment = "SELECT visits.visit_time,urls.url FROM visits LEFT OUTER JOIN urls ON visits.url = urls.id where visits.visit_time > ?;";

    /**
     * @param tempHistoryFile
     * @throws SQLException
     */
    public ChromeHistoryDAO(File tempHistoryFile) throws SQLException {
	connection = DriverManager.getConnection("jdbc:sqlite:" + tempHistoryFile.getPath());
    }

    /**
     * @return
     * @throws SQLException
     */
    public List<URLHistory> getHistory(Date startDate) throws SQLException {
	List<URLHistory> urls = new ArrayList<URLHistory>();
	try {
	    PreparedStatement pstmt = connection.prepareStatement(preparedStatment);
	    pstmt.setDate(1, getChromeRepresentasionOfTime(startDate));
	    resultSet = pstmt.executeQuery();
	    while (resultSet.next()) {
		URLHistory url = new URLHistory();
		url.setTime(new Date(getChromeTime(resultSet)));
		url.setUrl(resultSet.getString("url"));
		urls.add(url);
	    }
	} finally {
	    connection.close();
	}
	return urls;
    }

    /**
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private long getChromeTime(ResultSet resultSet) throws SQLException {
	return (((resultSet.getLong("visit_time") - 11644473600000000L) / 1000L));
    }

    private java.sql.Date getChromeRepresentasionOfTime(Date date) {
	long time = (date.getTime() * 1000L) + 11644473600000000L;
	return new java.sql.Date(time);
    }
}

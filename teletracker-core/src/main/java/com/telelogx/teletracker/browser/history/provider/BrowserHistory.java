package com.telelogx.teletracker.browser.history.provider;

import java.util.Date;
import java.util.List;

/**
 * @author Ahmad
 *
 */
public interface BrowserHistory {

    public void init();

    public List<URLHistory> getBrowserHistory(Date initTime);

    public Boolean getBrowserReadingHistoryAbility();

}

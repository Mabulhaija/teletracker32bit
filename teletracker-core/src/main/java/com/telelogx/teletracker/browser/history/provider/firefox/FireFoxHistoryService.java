package com.telelogx.teletracker.browser.history.provider.firefox;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.telelogx.teletracker.browser.history.provider.URLHistory;

/**
 * @author Ahmad
 *
 */
public class FireFoxHistoryService {

    private Connection connection;
    private ResultSet resultSet;

    private final String preparedStatment = "SELECT moz_historyvisits.visit_date,moz_places.url FROM moz_historyvisits LEFT OUTER JOIN moz_places  ON moz_historyvisits.place_id = moz_places.id where moz_historyvisits.visit_date > ?;";

    /**
     * @param tempHistoryFile
     * @throws SQLException
     */
    public FireFoxHistoryService(File tempHistoryFile) throws SQLException {
	connection = DriverManager.getConnection("jdbc:sqlite:" + tempHistoryFile.getPath());
    }

    /**
     * @return
     * @throws SQLException
     */
    public List<URLHistory> getHistory(Date startTime) throws SQLException {
	List<URLHistory> urls = new ArrayList<URLHistory>();
	try {
	    PreparedStatement pstmt = connection.prepareStatement(preparedStatment);
	    pstmt.setDate(1, getFireFoxDateRepresentation(startTime));
	    resultSet = pstmt.executeQuery();
	    while (resultSet.next()) {
		URLHistory url = new URLHistory();
		url.setTime(new Date(getFirefoxTime(resultSet)));
		url.setUrl(resultSet.getString("url"));
		urls.add(url);
	    }
	} finally {
	    connection.close();
	}
	return urls;
    }

    private java.sql.Date getFireFoxDateRepresentation(Date startTime) {
	long date = startTime.getTime() * 1000L;

	return new java.sql.Date(date);
    }

    /**
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private long getFirefoxTime(ResultSet resultSet) throws SQLException {
	return (((resultSet.getLong("visit_date")) / 1000L));
    }

}

package com.telelogx.teletracker.browser.history;

import java.util.Date;
import java.util.List;

import com.telelogx.teletracker.browser.history.provider.URLHistory;

/**
 * @author Ahmad
 *
 */
public interface BrowsersHistory {

    public void init();

    public List<URLHistory> getHistory();

    public List<URLHistory> getHistory(Date startTime);

    public List<URLHistory> getHistory(Date startTime, Date endTime);
}

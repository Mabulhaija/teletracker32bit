package com.telelogx.teletracker.browser.history.provider.exception;

public class BrowserHistoryException extends RuntimeException {

    /**
     * @param string
     * @param cause
     */
    public BrowserHistoryException(String string, Throwable cause) {
	super(string, cause);
    }

    public BrowserHistoryException(String string) {
	super(string);
    }

}

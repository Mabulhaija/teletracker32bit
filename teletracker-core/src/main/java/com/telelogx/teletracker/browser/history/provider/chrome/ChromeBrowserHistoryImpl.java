package com.telelogx.teletracker.browser.history.provider.chrome;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.telelogx.teletracker.browser.history.provider.BrowserHistory;
import com.telelogx.teletracker.browser.history.provider.URLHistory;
import com.telelogx.teletracker.browser.history.provider.exception.BrowserHistoryException;

/**
 * @author Ahmad
 *
 */
/**
 * @author Ahmad
 *
 */
public class ChromeBrowserHistoryImpl implements BrowserHistory {

    private Boolean isValid;
    private String userName;
    private String historyFilePath;
    private File historyFile;
    private File tempHistoryFile;
    private ChromeHistoryDAO chromeHistoryDAO;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 
     */
    public ChromeBrowserHistoryImpl() {
	historyFilePath = System.getProperty("user.home")
		+ "\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\History";
    }

    /**
     * @param historyFilePath
     */
    public ChromeBrowserHistoryImpl(String historyFilePath) {
	this.historyFilePath = historyFilePath;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.telelogx.project.browser.history.wrapper.BrowserHistoryWrapper#init()
     */
    @Override
    public void init() {
	logger.trace("Chrome driver start");
	try {
	    Class.forName("org.sqlite.JDBC");
	} catch (ClassNotFoundException e) {
	    throw new BrowserHistoryException("Faield to load SQLite driver", e);
	}
	checkChromeHistoryFile(historyFilePath);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.telelogx.project.browser.history.wrapper.BrowserHistoryWrapper#
     * getBrowserHistory()
     */
    @Override
    public List<URLHistory> getBrowserHistory(Date startTime) {
	List<URLHistory> urlsHistory = new ArrayList<URLHistory>();

	logger.trace("Chrome try to get history");
	try {
	    tempHistoryFile = createTmpHistoryFile(historyFile);
	} catch (IOException exception) {
	    logger.error("Faield while making copy for history file", exception);
	    throw new BrowserHistoryException("Faield while making copy for history file", exception.getCause());
	}
	try {
	    chromeHistoryDAO = new ChromeHistoryDAO(tempHistoryFile);
	    urlsHistory = chromeHistoryDAO.getHistory(startTime);
	} catch (SQLException e) {
	    logger.error("Faield while reading history file", e);
	    throw new BrowserHistoryException("Faield while reading history file", e);
	}
	return urlsHistory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.telelogx.project.browser.history.wrapper.BrowserHistoryWrapper#isValid
     * ()
     */
    @Override
    public Boolean getBrowserReadingHistoryAbility() {
	return isValid;
    }

    /**
     * @param sourceFile
     * @return
     * @throws IOException
     */
    private File createTmpHistoryFile(File sourceFile) throws IOException {
	tempHistoryFile = new File(sourceFile.toString() + "_tmp");
	Files.copy(sourceFile.toPath(), tempHistoryFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	return tempHistoryFile;
    }

    /**
     * @param historyFilePath2
     */
    private void checkChromeHistoryFile(String historyFilePath) {
	isValid = false;
	historyFile = new File(historyFilePath);
	if (historyFile.exists()) {
	    isValid = true;
	} else {
	    logger.error("Faield to read chrome history ,can't found chrome database in : " + historyFilePath + "");
	}
    }

    /**
     * @param args
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws ClassNotFoundException {

	ChromeBrowserHistoryImpl chromeBrowserHistoryWrapperImpl = new ChromeBrowserHistoryImpl("C:\\Users\\"
		+ System.getenv("USERNAME") + "\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\History");
	chromeBrowserHistoryWrapperImpl.init();
	System.out.println(chromeBrowserHistoryWrapperImpl.getBrowserReadingHistoryAbility());
	Date date = new Date();
	date.setHours(0);
	for (URLHistory urlHistory : chromeBrowserHistoryWrapperImpl.getBrowserHistory((date))) {
	    System.out.println(urlHistory.getTime());
	    System.out.println(urlHistory.getUrl());
	}

    }
}
